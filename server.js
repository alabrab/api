var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;



var path = require('path');

//var bodyParser = requiere("body-parser");

var requestjson = require('request-json');

var urlClientesMlab = "https://api.mlab.com/api/1/databases/alabra/collections/Clientes?apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt";
var urlUsuarioMlab =  "https://api.mlab.com/api/1/databases/alabra/collections/Usuarios?apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt";
var urlMLabRaiz = "https://api.mlab.com/api/1/databases/alabra/collections/";

var apiKey = "apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt";

var clienteMLabRaiz;

var usuarioMLabRaiz;

var clienteMLab = requestjson.createClient(urlClientesMlab);

var usuarioMLab = requestjson.createClient(urlUsuarioMlab);

var movimientosJSON = require('./movimientosv2.json');

var bodyparser = require('body-parser');

app.use(bodyparser.json());

app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");


  next();

});


app.listen(port);
console.log('todo list RESTful API server started on: ' + port);

//********************************************************************
//Sin uso
/*
app.get('/', function(req,res) {
  //res.send('Hola mundo NodeJS');
  res.sendFile(path.join(__dirname,'index.html'));
})

app.post('/', function(req, res) {
  res.send('Hemos recibido su petición POST cambiada');
})

app.put('/', function(req, res) {
  res.send('Hemos recibido su petición PUT');
})

app.delete('/', function(req, res) {
  res.send('Hemos recibido su petición DELETE');
})


app.get('/Clientes/:idcliente', function (req, res) {
  res.send('Aqui tiene al cliente: ' + req.params.idcliente);
})

app.get('/Movimientos', function (req, res) {
  res.sendfile('movimientosv1.json');
})

//Tratamiento de json
app.get('/v2/Movimientos', function (req, res) {
  res.json(movimientosJSON);
})

app.get('/v2/Movimientos/:id', function (req, res) {
  console.log(req.params.id);

  res.send(movimientosJSON[req.params.id-1]);
})

app.get('/v2/Movimientosq', function (req, res) {
  console.log(req.query);

  res.send('Recibido' + req.query);
});

app.post('/v2/Movimientos', function (req, res) {
  var nuevo = req.body;
  nuevo.id = movimientosJSON.length + 1;
  movimientosJSON.push(nuevo);
  res.send("Movimiento dado de alta");
});

*/
//********************************************
// Trae todos los registro de la colección de Clientes
//********************************************
app.get('/v1/Clientes', function (req, res) {
  clienteMLab.get('',function (err, resM, body) {
    if (err){
      console.log(body)
    } else {
      res.send(body);
    }
  });
});
//********************************************
// Trae un  registro de la colección de Clientes por email
//********************************************
app.post('/v1/Clientesem', function (req, res) {

var email = req.body.email;
//console.log('email dentro de llamada clientesem: ' + email);
var query = 'q={"email":"' + email + '"}';
  clienteMLab = requestjson.createClient(urlMLabRaiz + "Clientes?" + apiKey + "&" + query);
  clienteMLab.get('', function (err, resM, body) {
    if (!err){
        res.status(200).send(body);
        console.log(body);
      }else {
        res.status(400).send(err);
      }
    })
});
//********************************************
// Inserta nuevo registro completo en la colección de Clientes
//********************************************

app.post('/v1/Clientes', function (req, res) {
  clienteMLab.post('', req.body, function (err, resM, body) {
    res.send(body);

  });

});

//********************************************
//Genera nuevo registro en Usuarios
//********************************************
app.post('/v2/Newusr', function (req, res) {

    usuarioMLab.post('',req.body, function(err, resM, body) {
        res.send(body);
    });
});

//********************************************
//Verifica si el correo es único
//********************************************
app.post('/v2/GetNewUsr', function (req, res) {
  var email = req.body.email;
  var query = 'q={"email":"' + email + '"}';
  //console.log(req.body);
  //console.log(req.body.email);
  //console.log(query);
  usuarioMLab = requestjson.createClient(urlMLabRaiz + "Usuarios?" + apiKey + "&" + query);
  //console.log(urlMLabRaiz + "Usuarios?" + apiKey + "&" + query);
  usuarioMLab.get('', function (err, resM, body) {
    if (!err){
      if(body.length == 1 ){
        //Usuario ya existe
        res.status(400).send('El usuario (email) ya existe');
      }else {
        res.status(200).send('Nuevo usuario es válido');
      }
    }
  })

});

//********************************************
// Valida información para acceder al sistema
//********************************************

app.post('/v2/Login', function (req, res) {
  var email = req.body.email;
  var password = req.body.password;
  var query = 'q={"email":"' + email + '", "password":"' + password + '"}';
  //console.log(query);

  clienteMLabRaiz = requestjson.createClient(urlMLabRaiz + "Usuarios?" + apiKey + "&" + query);
  //console.log(urlMLabRaiz + "Usuarios?" + apiKey + "&" + query);
  clienteMLabRaiz.get('', function (err, resM, body) {
    if (!err){
      if(body.length == 1 ){
        //Login OK
        res.status(200).send('Validación usuario OK');
      }else {
        res.status(404).send('Usuario inexistente');
      }
    }
  });
});
